# README #

This is a record of the [30 Days to Learn jQuery](http://courses.tutsplus.com/courses/30-days-to-learn-jquery) course at [Tuts+](http://courses.tutsplus.com).

## 30 Days to Learn jQuery

### 1. Introduction
1. Welcome

### 2. The Basics
1. Hello jQuery
2. Not So Fast, jQuery
3. The Basics of Querying the DOM
4. Events 101
5. Events 201
6. Bind...Live...Delegate...Huh?
7. Creating and Appending Content

### 3. Effects
1. Slides and Structure
2. The this Keyword
3. Modifying Effect Speeds
4. Creating Custom Effect Methods
5. Full Control With animate
6. Homework Solutions
7. The Obligatory Slider (First Attempt)
8. Prototypal Inheritance and Refactoring the Slider
9. Your Questions Answered

### 4. Utilities
1. `$.each` and Templating
2. Say Hello to Handlebars
3. The Twitter API
4. Filtering with `jQuery.grep`

### 5. Custom Events
1. Custom Events and the Observer Pattern

### 6. AJAX
1. Loading Pages Asynchronously
2. Interacting with the Server-Side
3. PHP and jQuery: Part 1
4. PHP and jQuery: Part 2
5. Deferreds

### 7. Plugin Development
1. Head First Into Plugin Development

## Miscellaneous

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)